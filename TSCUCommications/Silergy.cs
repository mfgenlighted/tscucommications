﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TSCUCommications
{
    public class Silergy
    {
        private CUCommications cuCommications;
        const int CMD_RCV_DELAY = 5;

        // to save the ram to flash do these commands in this order
        byte[] wr_savetoflash1 = { 0x20, 0xF0, 0x01, 0xC2, 0x00, 0 };    // 0x0001   write
        byte[] wr_savetoflash2 = { 0x20, 0xF0, 0x02, 0x00, 0xAC, 0 };    // 0x0002   write
        byte[] wr_execute =      { 0x20, 0xF0, 0x00, 0x00, 0x00, 0 };    // 0x0000   write, execute the command



        public Silergy(CUCommications com)
        {
            cuCommications = com;
        }

        public void ClearCalibration(out bool errorOccured, out int errorCode, out string errorMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] sndData1 = { 0x20, 0xF0, 0x01, 0xC0, 0x00, 0 };  
            byte[] sndData2 = { 0x20, 0xF0, 0x02, 0x00, 0xAC, 0 };  

            try
            {
                cuCommications.send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(wr_execute, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                errorMsg = "Error doing 'Clear Calibration' functions. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.SystemErrors.SerialWriteError;
                errorOccured = true;
            }
        }

        public void SoftReset(out bool errorOccured, out int errorCode, out string errorMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] sndData1 = { 0x20, 0xF0, 0x01, 0x10, 0x00, 0 };
            byte[] sndData2 = { 0x20, 0xF0, 0x02, 0x00, 0xBD, 0 };

            try
            {
                cuCommications.send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(wr_execute, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                errorMsg = "Error doing 'Soft Reset' functions. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.SystemErrors.SerialWriteError;
                errorOccured = true;
            }
        }

        /// <summary>
        /// Will issue the command to save the cal and manufacturing data in RAM to Flash
        /// </summary>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public void SaveRamToFlash(out bool errorOccured, out int errorCode, out string errorMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            try
            {
                cuCommications.send_CU_com_port(wr_savetoflash1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(wr_savetoflash2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(wr_execute, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                errorMsg = "Error doing 'save ram to flash' functions. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
                errorOccured = true;
            }
        }

        /// <summary>
        /// Write a value to a register.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public void WriteRegValue(UInt32 value, UInt16 address, out bool errorOccured, out int errorCode, out string errorMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] sndData1 = { 0x20, 0xF0, 0x01, 0x00, 0x00, 0 };      // lsb
            byte[] sndData2 = { 0x20, 0xF0, 0x02, 0x00, 0x00, 0 };      // msb
            byte[] sndData3 = { 0x20, 0xF0, 0x00, 0x00, 0x00, 0 };      // 0x0000   write, execute the command

            // set reg value
            sndData1[4] = (byte)(value & 0xFF);
            sndData1[3] = (byte)((value >> 8) & 0xFF);
            sndData2[4] = (byte)((value >> 16) & 0xFF);
            sndData2[3] = (byte)((value >> 24) & 0xFF);


            // set reg address
            sndData3[3] = (byte)(address / 256);
            sndData3[4] = (byte)(address % 256);
            try
            {
                cuCommications.send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData3, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
            }
            catch (Exception ex)
            {
                errorMsg = "Error setting target voltage. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
                errorOccured = true;
            }
        }

        /// <summary>
        /// Write the Voltage target value to be use for calibration.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public void WriteVoltageTargetValue(double value, out bool errorOccured, out int errorCode, out string errorMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] sndData1 = { 0x20, 0xF0, 0x01, 0x00, 0x00, 0 };  // target * 10 in data
            byte[] sndData2 = { 0x20, 0xF0, 0x02, 0x00, 0x00, 0 };  // fixed 'set target voltage' command
            byte[] sndData3 = { 0x20, 0xF0, 0x00, 0x00, 0x5E, 0 };    // 0x0000   write, execute the command

            sndData1[3] = (byte)((value * 10) / 256);
            sndData1[4] = (byte)((value * 10) % 256);
            try
            {
                cuCommications.send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData3, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
            }
            catch (Exception ex)
            {
                errorMsg = "Error setting target voltage. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
                errorOccured = true;
            }
        }

        /// <summary>
        /// Write the Watt target value to be used for calibration
        /// </summary>
        /// <param name="value"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public void WriteWattTargetValue(double value, out bool errorOccured, out int errorCode, out string errorMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] sndData1 = { 0x20, 0xF0, 0x01, 0x00, 0x00, 0 };  // target * 10 in data
            byte[] sndData2 = { 0x20, 0xF0, 0x02, 0x00, 0x00, 0 };  // fixed 'set target voltage' command
            byte[] sndData3 = { 0x20, 0xF0, 0x00, 0x00, 0x5F, 0 };    // 0x0000   write, execute the command

            sndData1[3] = (byte)((value * 10) / 256);
            sndData1[4] = (byte)((value * 10) % 256);
            try
            {
                cuCommications.send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData3, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
            }
            catch (Exception ex)
            {
                errorMsg = "Error setting target voltage. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
                errorOccured = true;
            }
        }

        /// <summary>
        /// will do the calibration assuming that the target voltage and watts are loaded.
        /// Saves calibration if cal was good.
        /// </summary>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public void CalVotageAndWatts(int maxWaitTimeSec, out bool errorOccured, out int errorCode, out string errorMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] sndData1 = { 0x20, 0xF0, 0x01, 0x01, 0x20, 0 };
            byte[] sndData2 = { 0x20, 0xF0, 0x02, 0x00, 0xCA, 0 };
            byte[] readAdd1 = { 0x00, 0xF0, 0x00, 0x00, 0x00, 0 };
            byte[] readAdd2 = { 0x00, 0xF0, 0x01, 0x00, 0x00, 0 };
            byte[] readAdd3 = { 0x00, 0xF0, 0x02, 0x01, 0x20, 0 };
            DateTime startTime;
            bool calDone = false;

            try
            {
                // start the cal
                cuCommications.send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(wr_execute, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                // start watching for cal done
                startTime = DateTime.Now;
                do
                {
                    try
                    {
                        cuCommications.send_CU_com_port(readAdd1, 500, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                        cuCommications.send_CU_com_port(readAdd2, 500, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                        cuCommications.send_CU_com_port(readAdd3, 500, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                        if (cmdresponse[3] == 0)    // if done
                            calDone = true;
                    }
                    catch (Exception ex)
                    {
                        errorMsg = "Error waiting for cal to finish. Error message: " + ex.Message;
                        errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
                        errorOccured = true;
                        return;
                    }
                } while ((startTime.AddSeconds(maxWaitTimeSec) > DateTime.Now) && !calDone);
                if (calDone)
                {
                    SaveRamToFlash(out errorOccured, out errorCode, out errorMsg);
                }
                else
                    errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
            }
            catch (Exception ex)
            {
                errorMsg = "Error starting the cal. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
                errorOccured = true;
            }
        }

        /// <summary>
        /// Write the Relay timing values
        /// </summary>
        /// <param name="value"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public void WriteRelayZCTimingValues(uint SetTiming, uint RstTiming, bool SaveToFlash, out bool errorOccured, out int errorCode, out string errorMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] sndData1 = { 0x20, 0xF0, 0x01, 0x00, 0x00, 0 };
            byte[] sndData2 = { 0x20, 0xF0, 0x02, 0x00, 0x00, 0 };
            byte[] sndData3 = { 0x20, 0xF0, 0x00, 0x00, 0x00, 0 };

            sndData1[4] = (byte)(SetTiming & 0xFF);
            sndData1[3] = (byte)((SetTiming >> 8) & 0xFF);
            sndData2[4] = (byte)((SetTiming >> 16) & 0xFF);
            sndData3[4] = 0x94;
            try
            {
                cuCommications.send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData3, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
            }
            catch (Exception ex)
            {
                errorMsg = "Error setting relay set value. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
                errorOccured = true;
                return;
            }
            sndData1[4] = (byte)(RstTiming & 0xFF);
            sndData1[3] = (byte)((RstTiming >> 8) & 0xFF);
            sndData2[4] = (byte)((RstTiming >> 16) & 0xFF);
            sndData3[4] = 0x95;
            try
            {
                cuCommications.send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData3, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
            }
            catch (Exception ex)
            {
                errorMsg = "Error setting relay rst value. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
                errorOccured = true;
                return;
            }
            if (SaveToFlash)
                SaveRamToFlash(out errorOccured, out errorCode, out errorMsg);

        }

        public void WriteRelayZCMaskValues(uint SetMask, uint RstMask, bool SaveToFlash, out bool errorOccured, out int errorCode, out string errorMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] sndData1 = { 0x20, 0xF0, 0x01, 0x00, 0x00, 0 };
            byte[] sndData2 = { 0x20, 0xF0, 0x02, 0x00, 0x00, 0 };
            byte[] sndData3 = { 0x20, 0xF0, 0x00, 0x00, 0x00, 0 };

            sndData1[4] = (byte)(SetMask & 0xFF);
            sndData1[3] = (byte)((SetMask >> 8) & 0xFF);
            sndData2[4] = (byte)((SetMask >> 16) & 0xFF);
            sndData3[4] = 0x98;
            try
            {
                cuCommications.send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData3, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
            }
            catch (Exception ex)
            {
                errorMsg = "Error setting relay set mask value. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
                errorOccured = true;
                return;
            }
            sndData1[4] = (byte)(RstMask & 0xFF);
            sndData1[3] = (byte)((RstMask >> 8) & 0xFF);
            sndData2[4] = (byte)((RstMask >> 16) & 0xFF);
            sndData3[4] = 0x99;
            try
            {
                cuCommications.send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData3, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
            }
            catch (Exception ex)
            {
                errorMsg = "Error setting relay rst mask value. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
                errorOccured = true;
                return;
            }
            if (SaveToFlash)
                SaveRamToFlash(out errorOccured, out errorCode, out errorMsg);

        }

        public void WriteRelayPulseTimingValues(uint PulseLenght, uint OnDelay, bool SaveToFlash, out bool errorOccured, out int errorCode, out string errorMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] sndData1 = { 0x20, 0xF0, 0x01, 0x00, 0x00, 0 };
            byte[] sndData2 = { 0x20, 0xF0, 0x02, 0x00, 0x00, 0 };
            byte[] sndData3 = { 0x20, 0xF0, 0x00, 0x00, 0x00, 0 };

            sndData1[4] = (byte)(PulseLenght & 0xFF);
            sndData1[3] = (byte)((PulseLenght >> 8) & 0xFF);
            sndData2[4] = (byte)((PulseLenght >> 16) & 0xFF);
            sndData3[4] = 0x96;
            try
            {
                cuCommications.send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData3, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
            }
            catch (Exception ex)
            {
                errorMsg = "Error setting relay set value. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
                errorOccured = true;
                return;
            }
            sndData1[4] = (byte)(OnDelay & 0xFF);
            sndData1[3] = (byte)((OnDelay >> 8) & 0xFF);
            sndData2[4] = (byte)((OnDelay >> 16) & 0xFF);
            sndData3[4] = 0x97;
            try
            {
                cuCommications.send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
                cuCommications.send_CU_com_port(sndData3, 1000, CMD_RCV_DELAY, ref cmdresponse, out errorOccured, out errorCode, out errorMsg);
            }
            catch (Exception ex)
            {
                errorMsg = "Error setting relay rst value. Error message: " + ex.Message;
                errorCode = (int)ErrorCodes.TestErrors.FailedToPerformCUFunction;
            }
            if (SaveToFlash)
                SaveRamToFlash(out errorOccured, out errorCode, out errorMsg);

        }

    }
}
