﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSCUCommications
{
    class ErrorCodes
    {
        // Teststand error code ranges
        //  product errors: 5250 to 5274
        //  system errors: -8200 to -8224

        //---------------------
        // error codes
        //----------------------
        public enum TestErrors
        {
            Success,                    // 0  - success
            CommandWriteError = 5250,           // error in writing command to CU
            DidNotRcvCorrectNumOfBytes = 5251,   // the number of bytes recieved was the wrong
            CRCBad = 5252,                      // the CRC for the response is bad
            FailedToPerformCUFunction = 5253,   // gave a fail status when tried to do a cU function
             //FailedToGetCUData,                // was not able to get required data from the CU
           //ReadingOutsideLimts,        // 3  - reading is outside the limits
            //Current1OutsideLimits,      // 4  - led 1 current out of limits
            //Current2OutsideLimits,      // 5  - led 2 current out of limits
            //EffOutsideLimits,           // 6  - watt efficiency out of limits
            //DataMismatch,               // 7  - programed data does not match read back
            //RefusedByPFS,               // 8  - PFS refused this serial number
        }

        public enum SystemErrors
        {
            NoError,                        // 0 - no error
            SerialPortDefMissing = -8200,     // serial port name can not be blank
            SerialPortOpenError = -8201,        // error opening the serial port
            SerialPortNotOpen = -8202,          // serial port is not open
            SerialWriteError = -8203,           // expception thrown on command write
            //SystemErrorCodeStart = 1000000, // begining of the system error codes
            //SNCreationFail,                 // 1   - error trying to create a SN
            //LimitFileReadError,             // 2   - error reading the Limit file
            //NoTestsFoundForProduct,         // 3   - no tests where found in the limit file for the product
            //InvalidTest,                    // 4   - the test in the limit file is a invalid test
            //LabjackError,                   // 5   - error with the labjack
            //InvalidLimit,                   // 6   - limit is invalid
            //ProductOpenError,               // 7    - error opening the Product map database
            //ResultOpenError,                // 8    - error opening the Result database
            //NoProductCodeFound,             // 9    - the product code is not found in the database
            //MissingParameters,              // 10   - one or more parameters are missing for a test
            //InvalidOperation,               // 11   - test value operation invalid
            //InvalidTestType,                // 12  - invalid test type
            //InvalidOperationType,           // 13  - invalid operation type
            //InstrumentNotSetUp,             // 14  - the instrument has not been initialized
            //ACMeterReadError,               // 15  - the ac meter had a error reading
            //OperatorCancel,                  // 16  - operator canceled
            //FileOpenError,                  // 17  - error opening a file
            //LabelPrintError,                // 18   - label printing error
        }
    }
}
